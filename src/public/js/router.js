// Filename: router.js
define([
  'jquery',
  'underscore',
  'backbone',
  'react',
  'bootstrap',
  'UserModel',
  'vm'
], function ($, _, Backbone, React,Vm) {
  var AppRouter = Backbone.Router.extend({
      routes: {
      "league/add" : "addLeague",
      },


  addLeague: function(){

  },
  });

  var initialize = function(){

    var app_router = new AppRouter;


    //COMMENT : route boilerplate    
    // app_router.on('route:showProjects', function(){
   
    //     // Call render on the module we loaded in via the dependency array
    //     var projectsView = new ProjectsView();
    //     projectsView.render();

    // });

    Backbone.history.start();
  };
  return { 
    initialize: initialize
  };
});
